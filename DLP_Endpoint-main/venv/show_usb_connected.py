from ipaddress import ip_address
import json
import subprocess
from dataclasses import dataclass
from typing import Callable, List
from time import sleep
import telegram
import time
import win32print
import os
import socket
from winreg import *
import datetime
from datetime import timezone
from elasticsearch import Elasticsearch
import logging
import json

roots_hives = {
    "HKEY_CLASSES_ROOT": HKEY_CLASSES_ROOT,
    "HKEY_CURRENT_USER": HKEY_CURRENT_USER,
    "HKEY_LOCAL_MACHINE": HKEY_LOCAL_MACHINE,
    "HKEY_USERS": HKEY_USERS,
    "HKEY_PERFORMANCE_DATA": HKEY_PERFORMANCE_DATA,
    "HKEY_CURRENT_CONFIG": HKEY_CURRENT_CONFIG,
    "HKEY_DYN_DATA": HKEY_DYN_DATA
}

def parse_key(key):
    key = key.upper()
    parts = key.split('\\')
    root_hive_name = parts[0]
    root_hive = roots_hives.get(root_hive_name)
    partial_key = '\\'.join(parts[1:])

    if not root_hive:
        raise Exception('root hive "{}" was not found'.format(root_hive_name))

    return partial_key, root_hive


def get_sub_keys(key):
    partial_key, root_hive = parse_key(key)

    with ConnectRegistry(None, root_hive) as reg:
        with OpenKey(reg, partial_key) as key_object:
            sub_keys_count, values_count, last_modified = QueryInfoKey(key_object)
            try:
                for i in range(sub_keys_count):
                    sub_key_name = EnumKey(key_object, i)
                    yield sub_key_name
            except WindowsError:
                pass


def get_values(key, fields):
    partial_key, root_hive = parse_key(key)

    with ConnectRegistry(None, root_hive) as reg:
        with OpenKey(reg, partial_key) as key_object:
            data = {}
            for field in fields:
                try:
                    value, type = QueryValueEx(key_object, field)
                    data[field] = value
                except WindowsError:
                    pass

            return data


def get_value(key, field):
    values = get_values(key, [field])
    return values.get(field)


def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip_add=s.getsockname()[0]
    return ip_add



def get_pcname():
    key_pcname = r'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\ComputerName\ComputerName'
    value_pcname = get_values(key_pcname, ['ComputerName'])
    return value_pcname['ComputerName']




token1='5509609161:AAEJTRUFi9EAjuU4DCBVxH3W9i1XhuAt2tU'
CHAT_ID='-613250718'
def send_test_message(text1):
    try:
        telegram_notify = telegram.Bot(token1)
        message = text1
    
        telegram_notify.send_message(chat_id=CHAT_ID, text=message)
    except Exception as ex:
        print(ex)

    try:
        ELASTIC_HOST="http://14.232.152.36:9200/"
        # es = Elasticsearch(hosts=os.environ.get("ELASTIC_HOST"))
        es = Elasticsearch(hosts=ELASTIC_HOST)
        print(1)
        doc = {
                    "description": text1,
                    "@timestamp": datetime.datetime.now(timezone.utc).strftime("%Y-%m-%dT%H:%M:%S"),
                    "loai": "usb_connected",
                    "device_id": 0,
                    "ip_attack": ""
                }
        res = es.index(index="dlp_main", document=json.dumps(doc, indent=4, sort_keys=True, default=str))
        print(res)
    except Exception as ex:
        print(ex)
        logging.error("!!!!!!!!!!! Gửi lên ELK lỗi")   



@dataclass
class Drive:
    letter: str
    label: str
    drive_type: str

    @property
    def is_removable(self) -> bool:
        return self.drive_type == 'Removable Disk'


def list_drives() -> List[Drive]:
    """
    Get a list of drives using WMI
    :return: list of drives
    """
    proc = subprocess.run(
        args=[
            'powershell',
            '-noprofile',
            '-command',
            'Get-WmiObject -Class Win32_LogicalDisk | Select-Object deviceid,volumename,drivetype | ConvertTo-Json'
        ],
        text=True,
        stdout=subprocess.PIPE
    )
    if proc.returncode != 0 or not proc.stdout.strip():
        print('Failed to enumerate drives')
        return []
    devices = json.loads(proc.stdout)

    drive_types = {
        0: 'Unknown',
        1: 'No Root Directory',
        2: 'Removable Disk',
        3: 'Local Disk',
        4: 'Network Drive',
        5: 'Compact Disc',
        6: 'RAM Disk',
    }

    return [Drive(
        letter=d['deviceid'],
        label=d['volumename'],
        drive_type=drive_types[d['drivetype']]
    ) for d in devices]

def watch_drives(on_change: Callable[[List[Drive]], None], poll_interval: int = 1):
    prev = None
    while True:
        drives = list_drives()
        if prev != drives:
            on_change(drives)
            prev = drives
            for e in prev:
                if e.drive_type=='Removable Disk':
                    mes=("PC name: "+get_pcname()+" IP: "+get_ip()+"\nConnected Removable Disk:\n"+str(e))
                    send_test_message(mes)
        sleep(poll_interval)


if __name__ == '__main__':
    watch_drives(on_change=print)
    