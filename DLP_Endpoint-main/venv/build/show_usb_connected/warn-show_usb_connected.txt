
This file lists modules PyInstaller was not able to find. This does not
necessarily mean this module is required for running your program. Python and
Python 3rd-party packages include a lot of conditional or optional modules. For
example the module 'ntpath' only exists on Windows, whereas the module
'posixpath' only exists on Posix systems.

Types if import:
* top-level: imported at the top-level - look at these first
* conditional: imported within an if-statement
* delayed: imported within a function
* optional: imported within a try-except-statement

IMPORTANT: Do NOT post this list to the issue-tracker. Use it as a basis for
            tracking down the missing module yourself. Thanks!

missing module named 'win32com.gen_py' - imported by win32com (conditional, optional), E:\coder\AIS\DLP_Endpoint\venv\Lib\site-packages\PyInstaller\hooks\rthooks\pyi_rth_win32comgenpy.py (top-level)
missing module named org - imported by copy (optional)
missing module named grp - imported by shutil (delayed, optional), tarfile (optional), pathlib (delayed, optional), subprocess (delayed, conditional, optional)
missing module named pwd - imported by posixpath (delayed, conditional), shutil (delayed, optional), tarfile (optional), pathlib (delayed, optional), subprocess (delayed, conditional, optional), http.server (delayed, optional), webbrowser (delayed), netrc (delayed, conditional), getpass (delayed), distutils.util (delayed, conditional, optional)
missing module named urllib.pathname2url - imported by urllib (conditional), PyInstaller.lib.modulegraph._compat (conditional)
missing module named pep517 - imported by importlib.metadata (delayed)
missing module named 'org.python' - imported by pickle (optional), xml.sax (delayed, conditional)
missing module named posix - imported by os (conditional, optional), shutil (conditional), importlib._bootstrap_external (conditional)
missing module named resource - imported by posix (top-level), test.support (delayed, conditional, optional)
missing module named _frozen_importlib_external - imported by importlib._bootstrap (delayed), importlib (optional), importlib.abc (optional), zipimport (top-level)
excluded module named _frozen_importlib - imported by importlib (optional), importlib.abc (optional), zipimport (top-level), PyInstaller.loader.pyimod02_archive (delayed)
missing module named pyimod03_importers - imported by E:\coder\AIS\DLP_Endpoint\venv\Lib\site-packages\PyInstaller\hooks\rthooks\pyi_rth_pkgutil.py (top-level), E:\coder\AIS\DLP_Endpoint\venv\Lib\site-packages\PyInstaller\hooks\rthooks\pyi_rth_pkgres.py (top-level)
missing module named termios - imported by tty (top-level), getpass (optional)
missing module named readline - imported by cmd (delayed, conditional, optional), code (delayed, conditional, optional), pdb (delayed, optional)
missing module named __builtin__ - imported by pkg_resources._vendor.pyparsing (conditional)
missing module named ordereddict - imported by pkg_resources._vendor.pyparsing (optional)
missing module named _manylinux - imported by pkg_resources._vendor.packaging.tags (delayed, optional)
missing module named 'com.sun' - imported by pkg_resources._vendor.appdirs (delayed, conditional, optional)
missing module named com - imported by pkg_resources._vendor.appdirs (delayed)
missing module named _winreg - imported by platform (delayed, optional), tzlocal.win32 (optional), pkg_resources._vendor.appdirs (delayed, conditional)
missing module named 'pkg_resources.extern.pyparsing' - imported by pkg_resources._vendor.packaging.markers (top-level), pkg_resources._vendor.packaging.requirements (top-level)
missing module named pkg_resources.extern.packaging - imported by pkg_resources.extern (top-level), pkg_resources (top-level)
missing module named pkg_resources.extern.appdirs - imported by pkg_resources.extern (top-level), pkg_resources (top-level)
missing module named _scproxy - imported by urllib.request (conditional)
missing module named 'java.lang' - imported by platform (delayed, optional), xml.sax._exceptions (conditional)
missing module named vms_lib - imported by platform (delayed, optional)
missing module named java - imported by platform (delayed)
missing module named _posixshmem - imported by multiprocessing.resource_tracker (conditional), multiprocessing.shared_memory (conditional)
missing module named multiprocessing.BufferTooShort - imported by multiprocessing (top-level), multiprocessing.connection (top-level)
missing module named multiprocessing.AuthenticationError - imported by multiprocessing (top-level), multiprocessing.connection (top-level)
missing module named _posixsubprocess - imported by subprocess (optional), multiprocessing.util (delayed)
missing module named multiprocessing.get_context - imported by multiprocessing (top-level), multiprocessing.pool (top-level), multiprocessing.managers (top-level), multiprocessing.sharedctypes (top-level)
missing module named multiprocessing.TimeoutError - imported by multiprocessing (top-level), multiprocessing.pool (top-level)
missing module named multiprocessing.set_start_method - imported by multiprocessing (top-level), multiprocessing.spawn (top-level)
missing module named multiprocessing.get_start_method - imported by multiprocessing (top-level), multiprocessing.spawn (top-level)
missing module named asyncio.DefaultEventLoopPolicy - imported by asyncio (delayed, conditional), asyncio.events (delayed, conditional)
missing module named sets - imported by pytz.tzinfo (optional)
missing module named UserDict - imported by pytz.lazy (optional)
missing module named typing_extensions - imported by tornado.ioloop (conditional), tornado.platform.asyncio (conditional)
missing module named _curses - imported by curses (top-level), curses.has_key (top-level)
missing module named colorama - imported by tornado.log (optional)
missing module named 'backports.zoneinfo' - imported by tzlocal.unix (conditional)
missing module named 'typing.io' - imported by importlib.resources (top-level)
missing module named backports - imported by telegram.vendor.ptb_urllib3.urllib3.packages.ssl_match_hostname (optional), pytz_deprecation_shim._compat_py3 (optional), tzlocal.utils (optional)
missing module named dateutil - imported by pytz_deprecation_shim._compat_py2 (top-level)
missing module named twisted - imported by apscheduler.schedulers.twisted (optional)
missing module named PySide - imported by apscheduler.schedulers.qt (optional)
missing module named PyQt4 - imported by apscheduler.schedulers.qt (optional)
missing module named PyQt5 - imported by apscheduler.schedulers.qt (optional)
missing module named rethinkdb - imported by apscheduler.jobstores.rethinkdb (optional)
missing module named cPickle - imported by apscheduler.jobstores.sqlalchemy (optional), apscheduler.jobstores.redis (optional), apscheduler.jobstores.mongodb (optional), apscheduler.jobstores.zookeeper (optional), apscheduler.jobstores.rethinkdb (optional)
missing module named cStringIO - imported by cPickle (top-level)
missing module named copy_reg - imported by cPickle (top-level), cStringIO (top-level)
missing module named 'kazoo.client' - imported by apscheduler.jobstores.zookeeper (optional)
missing module named kazoo - imported by apscheduler.jobstores.zookeeper (top-level)
missing module named pymongo - imported by apscheduler.jobstores.mongodb (optional)
missing module named bson - imported by apscheduler.jobstores.mongodb (optional)
missing module named gevent - imported by apscheduler.schedulers.gevent (optional), apscheduler.executors.gevent (optional)
missing module named trollius - imported by apscheduler.util (optional), apscheduler.schedulers.asyncio (optional)
missing module named 'gevent.lock' - imported by apscheduler.schedulers.gevent (optional)
missing module named redis - imported by apscheduler.jobstores.redis (optional)
missing module named 'sqlalchemy.sql' - imported by apscheduler.jobstores.sqlalchemy (optional)
missing module named 'sqlalchemy.exc' - imported by apscheduler.jobstores.sqlalchemy (optional)
missing module named sqlalchemy - imported by apscheduler.jobstores.sqlalchemy (optional)
missing module named funcsigs - imported by apscheduler.util (optional)
missing module named StringIO - imported by PyInstaller.lib.modulegraph._compat (conditional), PyInstaller.lib.modulegraph.zipio (conditional), telegram.vendor.ptb_urllib3.urllib3.packages.six (conditional), six (conditional)
missing module named 'telegram.vendor.ptb_urllib3.urllib3.packages.six.moves' - imported by telegram.vendor.ptb_urllib3.urllib3.exceptions (top-level), telegram.vendor.ptb_urllib3.urllib3.connectionpool (top-level), telegram.vendor.ptb_urllib3.urllib3.connection (top-level), telegram.vendor.ptb_urllib3.urllib3.util.response (top-level), telegram.vendor.ptb_urllib3.urllib3.request (top-level), telegram.vendor.ptb_urllib3.urllib3.response (top-level), telegram.vendor.ptb_urllib3.urllib3.poolmanager (top-level), telegram.vendor.ptb_urllib3.urllib3.contrib.appengine (top-level)
missing module named _abcoll - imported by telegram.vendor.ptb_urllib3.urllib3.packages.ordered_dict (optional)
missing module named dummy_thread - imported by telegram.vendor.ptb_urllib3.urllib3.packages.ordered_dict (optional)
missing module named thread - imported by telegram.vendor.ptb_urllib3.urllib3.packages.ordered_dict (optional)
missing module named Queue - imported by telegram.vendor.ptb_urllib3.urllib3.connectionpool (conditional)
missing module named socks - imported by telegram.vendor.ptb_urllib3.urllib3.contrib.socks (optional)
missing module named 'urllib3.util' - imported by telegram.utils.request (optional)
missing module named 'urllib3.fields' - imported by telegram.utils.request (optional)
missing module named 'urllib3.connection' - imported by telegram.utils.request (optional)
missing module named 'urllib3.contrib' - imported by telegram.utils.request (optional)
missing module named urllib3 - imported by telegram.utils.request (optional)
missing module named google - imported by telegram.vendor.ptb_urllib3.urllib3.contrib.appengine (optional)
missing module named 'cryptography.hazmat' - imported by telegram.passport.credentials (optional), telegram.bot (optional)
missing module named ujson - imported by telegram.base (optional), telegram.utils.helpers (optional), telegram.passport.credentials (optional), telegram.bot (optional), telegram.utils.request (optional), telegram.ext.dictpersistence (optional), telegram.ext.utils.webhookhandler (optional)
missing module named cryptography - imported by telegram.passport.credentials (optional)
missing module named fcntl - imported by subprocess (optional)
