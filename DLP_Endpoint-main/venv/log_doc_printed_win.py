import time
import win32print
import os
import socket
import telegram
from winreg import *
import datetime
from datetime import timezone
from elasticsearch import Elasticsearch
import logging
import json
#----------------------------------------------------------------------

roots_hives = {
    "HKEY_CLASSES_ROOT": HKEY_CLASSES_ROOT,
    "HKEY_CURRENT_USER": HKEY_CURRENT_USER,
    "HKEY_LOCAL_MACHINE": HKEY_LOCAL_MACHINE,
    "HKEY_USERS": HKEY_USERS,
    "HKEY_PERFORMANCE_DATA": HKEY_PERFORMANCE_DATA,
    "HKEY_CURRENT_CONFIG": HKEY_CURRENT_CONFIG,
    "HKEY_DYN_DATA": HKEY_DYN_DATA
}

def parse_key(key):
    key = key.upper()
    parts = key.split('\\')
    root_hive_name = parts[0]
    root_hive = roots_hives.get(root_hive_name)
    partial_key = '\\'.join(parts[1:])

    if not root_hive:
        raise Exception('root hive "{}" was not found'.format(root_hive_name))

    return partial_key, root_hive


def get_sub_keys(key):
    partial_key, root_hive = parse_key(key)

    with ConnectRegistry(None, root_hive) as reg:
        with OpenKey(reg, partial_key) as key_object:
            sub_keys_count, values_count, last_modified = QueryInfoKey(key_object)
            try:
                for i in range(sub_keys_count):
                    sub_key_name = EnumKey(key_object, i)
                    yield sub_key_name
            except WindowsError:
                pass


def get_values(key, fields):
    partial_key, root_hive = parse_key(key)

    with ConnectRegistry(None, root_hive) as reg:
        with OpenKey(reg, partial_key) as key_object:
            data = {}
            for field in fields:
                try:
                    value, type = QueryValueEx(key_object, field)
                    data[field] = value
                except WindowsError:
                    pass

            return data


def get_value(key, field):
    values = get_values(key, [field])
    return values.get(field)


token1='5509609161:AAEJTRUFi9EAjuU4DCBVxH3W9i1XhuAt2tU'
CHAT_ID='-613250718'
def send_test_message(text1):
    try:
        telegram_notify = telegram.Bot(token1)
        message = text1
    
        telegram_notify.send_message(chat_id=CHAT_ID, text=message)
    except Exception as ex:
        print(ex)

    try:
        ELASTIC_HOST="http://14.232.152.36:9200/"
        # es = Elasticsearch(hosts=os.environ.get("ELASTIC_HOST"))
        es = Elasticsearch(hosts=ELASTIC_HOST)
        print(1)
        doc = {
                    "description": text1,
                    "@timestamp": datetime.datetime.now(timezone.utc).strftime("%Y-%m-%dT%H:%M:%S"),
                    "loai": "printed_win",
                    "device_id": 0,
                    "ip_attack": ""
                }
        res = es.index(index="dlp_main", document=json.dumps(doc, indent=4, sort_keys=True, default=str))
        print(res)
    except Exception as ex:
        print(ex)
        logging.error("!!!!!!!!!!! Gửi lên ELK lỗi")   



def print_job_checker():
    """
    Prints out all jobs in the print queue every 5 seconds
    """
    mes_send_tele=""
    jobs = [1]
    while 1:
        jobs = []
        for p in win32print.EnumPrinters(win32print.PRINTER_ENUM_LOCAL,
                                         None, 1):
            flags, desc, name, comment = p
            
            phandle = win32print.OpenPrinter(name)
            print_jobs = win32print.EnumJobs(phandle, 0, -1, 1)
            if print_jobs:
                jobs.extend(list(print_jobs))
            for job in print_jobs:
                mes_send_tele=get_pcname()
                mes_send_tele=mes_send_tele+"printer name => " + name+"\n"
                document = job["pDocument"]
                mes_send_tele= mes_send_tele+"Document name => " + document+"\n"
                time.sleep(3)
            
            send_test_message(mes_send_tele)
            time.sleep(5)
            win32print.ClosePrinter(phandle)
          
        time.sleep(5)
        #print ("No more jobs!")
    print ("No more jobs!")
        
#----------------------------------------------------------------------

def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip_add=s.getsockname()[0]
    return ip_add



def get_pcname():
    key_pcname = r'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\ComputerName\ComputerName'
    value_pcname = get_values(key_pcname, ['ComputerName'])
    mes_send_tele="PC_name "+value_pcname['ComputerName']+"\nIP "+get_ip()+'\nList document printing:\n'
    return mes_send_tele

if __name__ == "__main__":
    print_job_checker()