import typing
from json.tool import main
import sys
from warnings import catch_warnings
import textract
from  time import time
import re
import distro 
from shutil import which
import os
import logging
import datetime
from datetime import timezone
from elasticsearch import Elasticsearch

    
class ExtracPDF:
    
    def __init__(self) -> None:
        pass
    
    def is_tool():
    # """Check whether `name` is on PATH and marked as executable."""
        check_distro = True if distro.id() == "ubuntu" else False
        if which("tshark") and check_distro:
            # logging.info('[Checked]Tshark already installed in Ubuntu distro!') 
            print('[Checked]Tshark already installed in Ubuntu distro!') 
        else:
            # logging.info('Please install tshark before using this tool!')
            # logging.info('run : sudo apt install tshark -y')
            print('Please install tshark before using this tool!')
            print('run : sudo apt install tshark -y')
            sys.exit()
            
    def extract(pcap_path:str, dir:str, protocol: str):
        dir_pcap=pcap_path.split(".pcap")
        dir_pcap2=dir_pcap[0].split("/")
        lenx=len(dir_pcap2)
        pcap_name=dir_pcap2[lenx-1]
        #print(pcap_name)

        extracted_dir = f'{dir}/{pcap_name}'
        try:
            os.system(f'mkdir {extracted_dir}')
        except:
            print("")
        dir_name = str(int(time()))
        command = f'tshark -Q -r {pcap_path} --export-objects "{protocol},{extracted_dir}"'
        os.system(command)
        Non_PDF = f'{dir}/{dir_name}/files'
        PDF = f'{dir}/{dir_name}/files.pdf'
        if os.path.exists(Non_PDF):
            os.rename(Non_PDF, PDF)
            return PDF
        return None

#lay ma MD5 cua mot tep tin
import hashlib
def get_hash_md5(filename):
    md5_hash=hashlib.md5()
    with open(filename,'rb') as f:
        for byte_block in iter(lambda: f.read(4096),b""):
            md5_hash.update(byte_block)
        return(md5_hash.hexdigest())

# lay danh sach ma MD5 quan trong, can bao ve
def get_list_hash_protected(filename):
    file=open(filename,"r",encoding='utf8')
    content_file=file.readlines()
    list_hash=[]
    for i in content_file:
        list_hash.append(i.split("\t")[0])
    return list_hash

# lay danh sach ma MD5 cac tep tin trong luong mang
def export_hash_files(pathFolder):
    hash_files=[]
    for path, subdirs, files in os.walk(pathFolder):
        for name in files:
            pathfile=(os.path.join(path,name))
            #print(pathfile,"-",get_hash_md5(pathfile))
            hash_files.append(get_hash_md5(pathfile)+"\t"+pathfile)
    return hash_files

# kiem tra co xuat hien file quan trong
def check_hash_file_extracted(file_lish_hash_p, hash_files):
    list_alert=[]
    list_hash_file_trans=export_hash_files(hash_files)
    list_hash_protected=get_list_hash_protected(file_lish_hash_p)
    for i in list_hash_file_trans:
        
        for j in list_hash_protected:
            if i.split("\t")[0]==j:
                
                list_alert.append(i)
    return list_alert

import telegram
token1='5509609161:AAEJTRUFi9EAjuU4DCBVxH3W9i1XhuAt2tU'
CHAT_ID='-613250718'
def send_test_message(text1):
    try:
        telegram_notify = telegram.Bot(token1)
        message = text1
    
        telegram_notify.send_message(chat_id=CHAT_ID, text=message)
    except Exception as ex:
        print(ex)

    try:
            ELASTIC_HOST = "http://14.232.152.36:9200/"
            # es = Elasticsearch(hosts=os.environ.get("ELASTIC_HOST"))
            es = Elasticsearch(hosts=ELASTIC_HOST)
            doc = {
                "description": text1,
                "@timestamp": datetime.datetime.now(timezone.utc).strftime("%Y-%m-%dT%H:%M:%S"),
                "loai": "network_alert",
                "device_id": 0,
                "ip_attack": "",
            }

            res = es.index(index="dlp_main", document=doc)
            print(res)
    except Exception as ex:
        print(ex)
        logging.error("!!!!!!!!!!! Gửi lên ELK lỗi")


import os
import shutil   
def extract_allFolder(pathFolder):
    for path, subdirs, files in os.walk(pathFolder):
        for name in files:
            pathfile=(os.path.join(path,name))
            PDF = ExtracPDF.extract(pathfile, "DataExport", "http")
            shutil.move(pathfile,"Pcap_readed/")
    return None

if __name__ == "__main__":
    # ExtracPDF.is_tool()
    #PDF = ExtracPDF.extract("Pcap_examples/PDF_download.pcap", "DataExport", "http")
    #shutil.move("Pcap_examples/PDF_download.pcap","Pcap_readed/")





    extract_allFolder("Pcap_examples")
    mes_Alert=""
    for i in check_hash_file_extracted("listProtectedFile.txt","DataExport"):
        mes_Alert=mes_Alert+i+"\n"
    #print(mes_Alert)
    
    if mes_Alert!="":
        send_test_message("DLP Network Alert: "+mes_Alert)
        print(mes_Alert)
    
    import os, shutil
    folder = 'DataExport'
    for filename in os.listdir(folder):
        file_path = os.path.join(folder, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))

    

