import os
import shutil

source_folder = r"/tmp//"
destination_folder = r"/home/ubuntu6gb/Documents/Coder/DLP_Network_LABVN/Pcap_examples//"

# fetch all files
for file_name in os.listdir(source_folder):
    # construct full file path
    list_file_name = file_name.split(".")
    if len(list_file_name)>1:
        if file_name.split(".")[1]=="pcap":
            source = source_folder + file_name
            destination = destination_folder + file_name
            # copy only files
            if os.path.isfile(source):
                shutil.copy(source, destination)
                print('copied', file_name)