# DLP_Network_LABVN

Data loss preventation on network level

+ Bước 1: Chặn bắt gói tin mạng truyền tải trong hệ thống mạng (gồm mạng nội bộ và mạng Internet) được quản lý;
  --> Sử dụng Network TAP hoặc Port monitoring kết hợp với Wireshark/Tshark để chặn bắt gói tin.
+ Bước 2: Phân tích nội dung gói tin được chặn bắt và phục hồi tự động các dữ liệu không được mã hóa (gồm pdf, doc, excell, powerpoint, JPEG, PNG, Bitmap, GIF). Lưu trữ các dữ liệu này vào bộ nhớ tạm kèm theo thông tin về địa chỉ IP gửi/nhận, thời gian truyền tải phục vụ điều tra, truy vết.
  --> Sử dụng chức năng “Export Objects” của Wireshark/NetworkMiner để trích xuất các tệp tin từ nội dung gói tin pcap dạng tĩnh hoặc trực tiếp từ luồng mạng. hiện có thể bóc tách JPEG, PNG, Bitmap, GIF, txt, pdf, doc, excel, powerpoint.
  code: tshark -r PDF_download_https.pcap --export-objects http,./pdfs_ex
+ Bước 3: Trích xuất mã băm của các tệp tin luân chuyển chứa trong luồng mạng.
+ Bước 4: So khớp mã băm nằm trong Blacklist của tổ chức.
+ Bước 4: Cảnh báo các trường hợp vi phạm chính sách của Tập đoàn cho người chịu trách nhiệm với bằng chứng dữ liệu đã được phục hồi và thông tin xác minh liên quan (địa chỉ IP, thời gian,…) qua kênh Telegram.

Activate venv trong Ubuntu:
$ Sudo apt install python3.8-venv
$ python3 -m venv venv
$ source venv/bin/activate

Install packet in venv for DLP network:
sudo apt update
sudo apt install python3-pip && pip install --upgrade pip
sudo apt install python-dev libxml2-dev libxslt1-dev antiword unrtf poppler-utils pstotext tesseract-ocr flac ffmpeg lame libmad0 libsox-fmt-mp3 sox libjpeg-dev swig
pip install textract
pip install python-telegram-bot

pip3 install distro

pip3 install elasticsearch

pip3 install distro


sudo apt install tshark
