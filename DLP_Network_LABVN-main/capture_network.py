import time
from scapy.sendrecv import sniff
from scapy.utils import wrpcap
import threading as th

global my_keyword
my_keyword = 'EduSoft' #từ khoá trong payload cần lọc
global pkts
pkts = []
global tmp
tmp =2

def pkt_callback(pkt):
    global my_keyword
    global tmp
    if my_keyword in str(pkt):
        tmp=1
    else:
        tmp=0

def makecap(pkt):
    global pkts
    pkt_callback(pkt)
    if tmp==0:
        pkts.append(pkt)

def cap():
    global pkts
    sniff(prn= makecap,count=100) #cap 500 packet
    link = "Pcap_examples/"  # path out .pcap
    t = time.time_ns()
    out = link + str(t) + ".pcap"
    wrpcap(str(out), pkts)
    print("[ PCAP ]=>", out)
    pkts = []

keep_going = True
def key_capture_thread():
    global keep_going
    input()
    keep_going = False

def do_stuff():
    th.Thread(target=key_capture_thread, args=(), name='key_capture_thread', daemon=True).start()
    while keep_going:
        print("Enter để dừng")
        cap()

do_stuff()