from django.apps import AppConfig


class DlpMainConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'dlp_main'
