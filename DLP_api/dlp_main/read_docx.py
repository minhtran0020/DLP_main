#Chương trình tự động phân loại và đánh dấu các tệp tin cần bảo vệ lưu trữ trong ổ cứng
from cgitb import text
import docx
import os
import io
from pdfminer3.layout import LAParams, LTTextBox
from pdfminer3.pdfpage import PDFPage
from pdfminer3.pdfinterp import PDFResourceManager
from pdfminer3.pdfinterp import PDFPageInterpreter
from pdfminer3.converter import PDFPageAggregator
from pdfminer3.converter import TextConverter
# Import openyxl module
import openpyxl
import hashlib
import datetime
from datetime import timezone
import logging
from elasticsearch import Elasticsearch

def getExtendFile(filename):
    exFile = filename.split(".")
    a=len(exFile)
    return exFile[a-1]

def get_content_excel(pathFile):
    # Define variable to load the wookbook
    if (getExtendFile(pathFile) =="xlsx") and((os.path.getsize(pathFile))>0):
        wookbook = openpyxl.load_workbook(pathFile)
        fullText = []

        # Define variable to read the active sheet:
        worksheet = wookbook.active

        # Iterate the loop to read the cell values
        for i in range(0, worksheet.max_row):
            for col in worksheet.iter_cols(1, worksheet.max_column):
                #print(col[i].value, end="\t\t")
                fullText.append(str(col[i].value))
        return fullText
    else:
        return ""

from pptx import Presentation
def getTextPptx(filename):
    # get text from pptx document
    prs = Presentation(filename)
    fullText=[]
    for slide in prs.slides:
        for shape in slide.shapes:
            if not shape.has_text_frame:
                continue
            for paragraph in shape.text_frame.paragraphs:
                for run in paragraph.runs:
                    fullText.append(run.text)
    return fullText


#get text from pdf document

def getTextPdf(filename):
    resource_manager = PDFResourceManager()
    fake_file_handle = io.StringIO()
    converter = TextConverter(resource_manager, fake_file_handle, laparams=LAParams())
    page_interpreter = PDFPageInterpreter(resource_manager, converter)

    with open(filename, 'rb') as fh:

        for page in PDFPage.get_pages(fh,
                                    caching=True,
                                    check_extractable=True):
            page_interpreter.process_page(page)

        text = fake_file_handle.getvalue()

    # close open handles
    converter.close()
    fake_file_handle.close()
    return(text)




def getTextdocx(filename):
    # get text from docx document
    if (getExtendFile(filename) =="docx") and((os.path.getsize(filename))>0):
        doc = docx.Document(filename)
        fullText = []
        for para in doc.paragraphs:
            fullText.append(para.text)
        return '\n'.join(fullText)
    # get text from xlsx document
    elif (getExtendFile(filename) =="xlsx") and((os.path.getsize(filename))>0):
        wookbook = openpyxl.load_workbook(filename)
        fullText = []

        # Define variable to read the active sheet:
        worksheet = wookbook.active

        # Iterate the loop to read the cell values
        for i in range(0, worksheet.max_row):
            for col in worksheet.iter_cols(1, worksheet.max_column):
                #print(col[i].value, end="\t\t")
                fullText.append(str(col[i].value))
        return fullText
    # get text from pptx document
    elif(getExtendFile(filename) =="pptx") and((os.path.getsize(filename))>0):
        prs = Presentation(filename)
        fullText=[]
        for slide in prs.slides:
            for shape in slide.shapes:
                if not shape.has_text_frame:
                    continue
                for paragraph in shape.text_frame.paragraphs:
                    for run in paragraph.runs:
                        fullText.append(run.text)
        return fullText
    # get text from pdf document
    elif(getExtendFile(filename) =="pdf") and((os.path.getsize(filename))>0):
        return(getTextPdf(filename))
    else:
        return ""
        


def findKeyword(keyword,filename):
    input_doc_str=str(getTextdocx(filename)).upper()
    if input_doc_str.find(keyword.upper())>=0:
        return True
    else: 
        return False

def read_keywords(filename):
    with io.open(filename,'r',encoding='utf8') as f:
        text=f.read()
    return text.split("\n")

#Kiểm tra tệp tin filecheck có từ khóa chứa trong filekeywords
def checkPolicyKeyword(filekeywords,filecheck):
    status_check=""
    for e in read_keywords(filekeywords):
        if findKeyword(e,filecheck):
            status_check=e
    return status_check


def get_hash_md5(filename):
    md5_hash=hashlib.md5()
    with open(filename,'rb') as f:
        for byte_block in iter(lambda: f.read(4096),b""):
            md5_hash.update(byte_block)
        return(md5_hash.hexdigest())


# Xuất danh sách các tệp tin cần bảo vệ có trong thư mục pathFolder ra tệp tin pathFileList
def exportListProtectedFile(pathFolder,pathFileList):
    output_file=open(pathFileList,"r",encoding='utf8')
    current_list_files=output_file.readlines()
        

    output_file.close()
    output_file=open(pathFileList,"a",encoding='utf8')
    for path, subdirs, files in os.walk(pathFolder):
        for name in files:
            pathfile=(os.path.join(path,name))
            #print(pathfile, checkPolicyKeyword("keyword.txt",pathfile),get_hash_md5(pathfile))
            keyw=checkPolicyKeyword("dlp_main/keyword.txt",pathfile)
            if keyw!="":
                out1=get_hash_md5(pathfile)+"\t"+pathfile+"\t ("+keyw+")\n"
                if not(out1 in current_list_files):
                    output_file.write(get_hash_md5(pathfile)+"\t"+pathfile+"\t ("+keyw+")\n")
                    try:
                        ELASTIC_HOST = "http://14.232.152.36:9200/"
                        # es = Elasticsearch(hosts=os.environ.get("ELASTIC_HOST"))
                        es = Elasticsearch(hosts=ELASTIC_HOST)
                        doc = {
                            "description": get_hash_md5(pathfile)+"\t"+pathfile+"\t ("+keyw+")\n",
                            "@timestamp": datetime.datetime.now(timezone.utc).strftime("%Y-%m-%dT%H:%M:%S"),
                            "loai": "protected_file",
                            "device_id": 0,
                            "ip_attack": "",
                        }

                        res = es.index(index="dlp_main", document=doc)
                        print(res)
                    except Exception as ex:
                        print(ex)
                        logging.error("!!!!!!!!!!! Gửi lên ELK lỗi")
    output_file.close()

# exportListProtectedFile("inputFolder","listProtectedFile.txt")