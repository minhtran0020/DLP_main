from django.urls import path, include
from .import views
urlpatterns= [
    path('storage',views.Storage.as_view()),
    path('extract_files',views.ExtractFilesFromPcap.as_view()),
    path('capture_network',views.CaptureNetwork.as_view()),
]