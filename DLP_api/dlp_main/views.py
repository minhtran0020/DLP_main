from django.shortcuts import render
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from .import extract_files_frompcap
from rest_framework.exceptions import AuthenticationFailed
from . import main_for_api
from . import capture_network
from auth_user import models
from auth_user import serializers
import jwt,datetime

class ExtractFilesFromPcap(APIView):
    def get(self,request):
        if check_token(request):
            try:
                data=extract_files_frompcap.main_extract_files_frompcap()
                return Response(data={'data':data,'status':True},status=status.HTTP_200_OK)
            except:
                return Response(data={'status':False},status=status.HTTP_400_BAD_REQUEST)
        else:
            raise AuthenticationFailed("Unauthenticated")

    
class CaptureNetwork(APIView):
    def get(sefl,request):
        if check_token(request):
            try:
                capture_network.cap()
                return Response(data={"status":True},status=status.HTTP_200_OK)
            except:
                return Response(data={'msg':"ERROR FROM NETWORK",'status':False},status=status.HTTP_400_BAD_REQUEST)
        else:
            raise AuthenticationFailed("Unauthenticated")
        
class Storage(APIView):
    def post(self,request,*arg,**kwargs):
        if check_token(request):
            folder_in=''
            file_out=''
            try:
                folder_in = request.data['folder_in']
                file_out = request.data['file_out']
            except:
                return Response(data="No file or folder",status=status.HTTP_400_BAD_REQUEST)
            try:
                main_for_api.tick_file_have_key(folder_in,file_out)
                data_out = main_for_api.get_file_have_key(file_out)
                return Response(data={'file':data_out,'status':True},status=status.HTTP_200_OK)
            except:
                return Response(data={'msg':"DLP_storate Inactive",'status':False},status=status.HTTP_400_BAD_REQUEST)
        else:
            raise AuthenticationFailed("Unauthenticated")
def check_token(request):
    token = request.COOKIES.get('jwt')
    if not token:
        return True
    try:
        payload = jwt.decode(token,'secret',algorithms=["HS256"])
    except jwt.ExpiredSignatureError:
        return True
    auth_user = models.AuthUser.objects.filter(id=payload['id']).first()
    serializer = serializers.AuthUserSerializers(auth_user)
    if auth_user:
        return serializer.data
    return True