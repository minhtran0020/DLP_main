from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import AuthenticationFailed
from rest_framework import status
import jwt,datetime
from dlp_main import views

from .serializers import *
from .models import *
# Create your views here.
class Register(APIView):
    def post(self,request):
        serializers = AuthUserSerializers(data=request.data)
        serializers.is_valid(raise_exception=True)
        serializers.save()
        return Response(serializers.data)
    
class LoginView(APIView):
    def post(self, request):
        username = request.data['username']
        password = request.data['password']

        auth_user = AuthUser.objects.filter(username=username).first()
        if auth_user is None:
            raise AuthenticationFailed('User not found')
        if not auth_user.check_password(password):
            raise AuthenticationFailed('Incorrect password')
        payload={
            'id':auth_user.id,
            'exp':datetime.datetime.utcnow()+datetime.timedelta(minutes=60),
            'iat':datetime.datetime.utcnow()
        }

        token = jwt.encode(payload,'secret',algorithm="HS256")

        response = Response()
        response.set_cookie(key='jwt',value=token,httponly=True)
        response.data={
            'jwt':token
        }
        response.status=status.HTTP_200_OK

        return response


class AuthUserView(APIView):
    def get(self,request):
        check_token =views.check_token(request)
        if check_token:
            return Response(data={'file':check_token,'status':True},status=status.HTTP_200_OK)
        else:   raise AuthenticationFailed("Unauthenticated")