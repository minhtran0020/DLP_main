from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.
class AuthUser(AbstractUser):
    password = models.CharField(max_length=128)
    # last_login = models.DateTimeField(blank=True, null=True)
    # is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    # is_staff = models.BooleanField()
    is_active = models.BooleanField()
    # date_joined = models.DateTimeField()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []